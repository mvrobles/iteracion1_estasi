package EPSAndes.negocio;

import java.sql.Date;

public class Reserva_Servicios {

	//------------------------------
	//Atributos
	//------------------------------
	
	private Integer idAfiliado;
	private Integer idServicio;
	private Integer asistio;
	private Date fechaServicio;
	
	//------------------------------
	//Metodos
	//------------------------------
	
	/**
	 * Constructor Reserva_Servicios
	 * @param idAfiliado
	 * @param idServicio
	 * @param asistio
	 * @param frchaServicio
	 */
	public Reserva_Servicios(Integer idAfiliado, Integer idServicio, Integer asistio, Date frchaServicio)
	{
		setAsistio(asistio);
		setFechaServicio(frchaServicio);
		setIdAfiliado(idAfiliado);
		setIdServicio(idServicio);
	}

	public Integer getIdAfiliado() {
		return idAfiliado;
	}

	public void setIdAfiliado(Integer idAfiliado) {
		this.idAfiliado = idAfiliado;
	}

	public Integer getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(Integer idServicio) {
		this.idServicio = idServicio;
	}

	public Integer getAsistio() {
		return asistio;
	}

	public void setAsistio(Integer asistio) {
		this.asistio = asistio;
	}

	public Date getFechaServicio() {
		return fechaServicio;
	}

	public void setFechaServicio(Date fechaServicio) {
		this.fechaServicio = fechaServicio;
	}
	
}
