package EPSAndes.negocio;

import java.sql.Date;

public class Afiliado {

	//----------------------------------
	//Atributos
	//----------------------------------
	
	private Integer idUsuario;
	private Date fechaDeNacimiento;
	
	//----------------------------------
	//Metodos
	//----------------------------------
	
	/**
	 * Constructor de la clase Afiliado
	 * @param nombre
	 * @param identificacion
	 * @param fechaDeNacimiento tipo Date
	 */
	public Afiliado(Integer idUsuario,Date fechaDeNacimiento) {
		this.setIdUsuario(idUsuario);
		this.setFechaDeNacimiento(fechaDeNacimiento);
	}

	public Date getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}

	public void setFechaDeNacimiento(Date fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

}
