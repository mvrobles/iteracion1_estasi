package EPSAndes.negocio;

public class AdministradorDeDatos {

	//-----------------------------------
	//Atributos
	//-----------------------------------
	
	private Integer idUsuario;
	
	//-----------------------------------
	//Metodos
	//-----------------------------------
	
	/**
	 * Constructor de AdministradorDeDatos
	 * @param idUsuario
	 */
	public AdministradorDeDatos(Integer idUsuario)
	{
		setIdUsuario(idUsuario);
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

}
