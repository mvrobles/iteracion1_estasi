package EPSAndes.negocio;

public class Medicamento {

	//---------------------------------
	//Atributos
	//---------------------------------
	
	private Integer idMedicamento;
	private String nombre;
	
	//---------------------------------
	//Metodos
	//---------------------------------
	
	public Medicamento (String nombre, Integer idMedicamento){
		setNombre(nombre);
		setIdMedicamento(idMedicamento);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getIdMedicamento() {
		return idMedicamento;
	}

	public void setIdMedicamento(Integer idMedicamento) {
		this.idMedicamento = idMedicamento;
	}
	
}
