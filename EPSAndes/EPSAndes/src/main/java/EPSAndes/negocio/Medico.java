package EPSAndes.negocio;

public class Medico {

	//----------------------------------------------------
	//Atributos
	//----------------------------------------------------
	
	private Integer idPersona; //FK a Usuarios 
	private String especialidad;
	private Integer numeroRegistroMedico;
	
	//-----------------------------------------------------
	//Metodos
	//-----------------------------------------------------

	/**
	 * Constructor de la clase Medico
	 * @param nombre
	 * @param identificacion
	 * @param especialidad
	 * @param numeroRegistroMedico
	 * @param iPS ArrayList de todas las ips a las que pertenece
	 */
	public Medico(Integer idPersona, String especialidad, Integer numeroRegistroMedico, Integer idIPS) {
		this.setIdPersona(idPersona);
		this.setEspecialidad(especialidad);
		this.setNumeroRegistroMedico(numeroRegistroMedico);
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	public Integer getNumeroRegistroMedico() {
		return numeroRegistroMedico;
	}

	public void setNumeroRegistroMedico(Integer numeroRegistroMedico) {
		this.numeroRegistroMedico = numeroRegistroMedico;
	}

	public Integer getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Integer idPersona) {
		this.idPersona = idPersona;
	}

}
