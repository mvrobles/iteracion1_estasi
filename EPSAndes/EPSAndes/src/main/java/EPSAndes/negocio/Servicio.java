package EPSAndes.negocio;

public class Servicio {
	
	//-----------------------------
	//Enumeracion
	//-----------------------------
	
	public enum TipoServicio{
		CONSULTA,
		UGENCIAS,
		REMISION,
		CONTROL,
		EXAMEN_DIAGNOSTICO,
		TERAPIA,
		PROCEDIMIENTO,
		HOSPITALIZACION
	}
	
	//------------------------------
	//Atributos
	//------------------------------
	
	private Integer idServicio;
	private Integer capacidad;
	private TipoServicio tipoDeServicio;
	
	//------------------------------
	//Metodos
	//------------------------------
	
	public Servicio (Integer idServicio, Integer capacidad, TipoServicio tipoDeServicio)
	{
		setIdServicio(idServicio);
		setCapacidad(capacidad);
		setTipoDeServicio(tipoDeServicio);
	}

	public Integer getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(Integer idServicio) {
		this.idServicio = idServicio;
	}

	public Integer getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(Integer capacidad) {
		this.capacidad = capacidad;
	}

	public TipoServicio getTipoDeServicio() {
		return tipoDeServicio;
	}

	public void setTipoDeServicio(TipoServicio tipoDeServicio) {
		this.tipoDeServicio = tipoDeServicio;
	}

}
