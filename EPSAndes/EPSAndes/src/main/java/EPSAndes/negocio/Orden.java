package EPSAndes.negocio;

import java.sql.Date;

public class Orden {
	
	//-------------------------------
	//Atributos
	//-------------------------------
	
	private Integer idOrden;
	private Date fecha;
	private Integer idServicio;
	private Integer idAfiliado;
	private Integer idMedico;
	
	//-------------------------------
	//Metodos
	//-------------------------------
	
	/**
	 * Constructor de la clase Orden
	 * @param idOrden
	 * @param fecha
	 * @param idServicio
	 * @param idAfiliado
	 * @param idMedico
	 */
	public Orden (Integer idOrden, Date fecha, Integer idServicio, Integer idAfiliado, Integer idMedico)
	{
		setFecha(fecha);
		setIdAfiliado(idAfiliado);
		setIdMedico(idMedico);
		setIdOrden(idOrden);
		setIdServicio(idServicio);
	}

	public Integer getIdOrden() {
		return idOrden;
	}

	public void setIdOrden(Integer idOrden) {
		this.idOrden = idOrden;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Integer getIdAfiliado() {
		return idAfiliado;
	}

	public void setIdAfiliado(Integer idAfiliado) {
		this.idAfiliado = idAfiliado;
	}

	public Integer getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(Integer idServicio) {
		this.idServicio = idServicio;
	}

	public Integer getIdMedico() {
		return idMedico;
	}

	public void setIdMedico(Integer idMedico) {
		this.idMedico = idMedico;
	}

}
