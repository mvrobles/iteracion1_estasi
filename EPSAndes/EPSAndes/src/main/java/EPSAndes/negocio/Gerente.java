package EPSAndes.negocio;

public class Gerente {
	//-----------------------------------
	//Atributos
	//-----------------------------------

	private Integer idUsuario;

	//-----------------------------------
	//Metodos
	//-----------------------------------

	/**
	 * Constructor de Gerente
	 * @param idUsuario
	 */
	public Gerente(Integer idUsuario)
	{
		setIdUsuario(idUsuario);
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

}
