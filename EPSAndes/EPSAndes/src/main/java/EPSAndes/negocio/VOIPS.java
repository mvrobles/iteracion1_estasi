package EPSAndes.negocio;

public interface VOIPS {

	public String getNombre();

	public String getLocalizacion(); 

	public void setNombre(String nombre);

	public void setLocalizacion(String localizacion); 

	public void setIdIPS(Integer idIPS);
	
	public Integer getIdIPS();

}
