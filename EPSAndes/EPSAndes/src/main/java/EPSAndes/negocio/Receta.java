package EPSAndes.negocio;

public class Receta {

	//--------------------------------
	//Atributos
	//--------------------------------
	
	private Integer idReceta;
	private Integer idMedico;
	private Integer idMedicamento;
	private Integer idAfiliado;
	private String cantidad;
	private String frecuencia;
	
	//-------------------------------
	//Metodos
	//-------------------------------
	
	/**
	 * Constructor de la clase Receta
	 * @param cantidad
	 * @param frecuencia
	 * @param medicamento
	 */
	public Receta (Integer idReceta, Integer idMedico, Integer idMedicamento, Integer idAfiliado, String cantidad, String frecuencia)
	{
		this.setIdReceta(idReceta);
		this.setIdMedico(idMedico);
		this.setIdMedicamento(idMedicamento);
		this.setIdAfiliado(idAfiliado);
		this.setCantidad(cantidad);
		this.setFrecuencia(frecuencia);
	}

	public String getFrecuencia() {
		return frecuencia;
	}

	public void setFrecuencia(String frecuencia) {
		this.frecuencia = frecuencia;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getIdReceta() {
		return idReceta;
	}

	public void setIdReceta(Integer idReceta) {
		this.idReceta = idReceta;
	}

	public Integer getIdMedico() {
		return idMedico;
	}

	public void setIdMedico(Integer idMedico) {
		this.idMedico = idMedico;
	}

	public Integer getIdMedicamento() {
		return idMedicamento;
	}

	public void setIdMedicamento(Integer idMedicamento) {
		this.idMedicamento = idMedicamento;
	}

	public Integer getIdAfiliado() {
		return idAfiliado;
	}

	public void setIdAfiliado(Integer idAfiliado) {
		this.idAfiliado = idAfiliado;
	}
}
