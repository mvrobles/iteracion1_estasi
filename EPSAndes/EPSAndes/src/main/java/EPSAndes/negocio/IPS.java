package EPSAndes.negocio;

public class IPS implements VOIPS{

	//----------------------------------------------------
	//Atributos
	//----------------------------------------------------

	private Integer idIPS;
	private String nombre;
	private String localizacion;

	//----------------------------------------------------
	//Metodos
	//----------------------------------------------------


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getLocalizacion() {
		return localizacion;
	}

	public void setLocalizacion(String localizacion) {
		this.localizacion = localizacion;
	}

	public Integer getIdIPS() {
		return idIPS;
	}

	public void setIdIPS(Integer idIPS) {
		this.idIPS = idIPS;
	}

}
