package EPSAndes.negocio;

public class Recepcionista{

	//----------------------------
	//Atributos
	//----------------------------
	
	private Integer idRecepcionista; //FK a Usuarios
	private Integer idIPS;
	
	//----------------------------
	//Metodos
	//----------------------------
	
	/**
	 * Constructor de la clase Recepcionista
	 * @param nombre
	 * @param identificacion
	 * @param idIPS
	 */
	public Recepcionista(Integer idRecepcionista, Integer idIPS) {
		this.idRecepcionista = idRecepcionista;
		this.idIPS = idIPS;
	}

	public Integer getidIPS() {
		return idIPS;
	}

	public void setidIPS(Integer idIPS) {
		this.idIPS = idIPS;
	}

	public Integer getIdRecepcionista() {
		return idRecepcionista;
	}

	public void setIdRecepcionista(Integer idRecepcionista) {
		this.idRecepcionista = idRecepcionista;
	}
}
