package EPSAndes.negocio;

public class Usuario {

	//---------------------------------
	//Atributos
	//---------------------------------
	/**
	 * Enumeraci�n del tipo de documento. Tipos de documentos permitidos
	 *
	 */
	public enum TipoDocumento {
		TI,
		CC,
		CE
	}


	//Atributos
	private long numeroDocumento;
	private String correoElectronico;
	private TipoDocumento tipoDocumento;
	private String nombre;


	//---------------------------------
	//Metodos
	//---------------------------------

	/**
	 * Constructor de la clase Usuario
	 * @param nombre de tipo String
	 * @param identificacion de tipo Identificacion
	 */
	public Usuario (String nombre, Integer numeroDocumento, TipoDocumento tipoDocumento, String correoElectronico)
	{
		this.setNombre(nombre);
		this.setNumeroDocumento(numeroDocumento);
		this.setTipoDocumento(tipoDocumento);
		this.setCorreoElectronico(correoElectronico);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public long getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(long numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

}
