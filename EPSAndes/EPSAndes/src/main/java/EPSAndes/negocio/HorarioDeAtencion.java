package EPSAndes.negocio;

import java.sql.Date;

public class HorarioDeAtencion {

	//--------------------------------
	//Enumeracion
	//--------------------------------
	
	public enum DiasQueAplica{
		LUNES,
		MARTES,
		MIERCOLES,
		JUEVES,
		VIERNES,
		SABADO,
		DOMINGO,
		FESTIVO
	}
	
	//--------------------------------
	//Atributos
	//--------------------------------
	
	private Integer idHorario;
	private Date horaDeInicio;
	private Date horaDeFin;
	private DiasQueAplica diasQueAplica;
	
	//--------------------------------
	//Metodos
	//--------------------------------
	
	public HorarioDeAtencion(Integer idHorario, Date horaDeInicio, Date horaDeFin, DiasQueAplica diasQueAplica)
	{
		setIdHorario(idHorario);
		setHoraDeInicio(horaDeInicio);
		setHoraDeFin(horaDeFin);
		setDiasQueAplica(diasQueAplica);
	}

	public Integer getIdHorario() {
		return idHorario;
	}

	public void setIdHorario(Integer idHorario) {
		this.idHorario = idHorario;
	}

	public Date getHoraDeInicio() {
		return horaDeInicio;
	}

	public void setHoraDeInicio(Date horaDeInicio) {
		this.horaDeInicio = horaDeInicio;
	}

	public Date getHoraDeFin() {
		return horaDeFin;
	}

	public void setHoraDeFin(Date horaDeFin) {
		this.horaDeFin = horaDeFin;
	}

	public DiasQueAplica getDiasQueAplica() {
		return diasQueAplica;
	}

	public void setDiasQueAplica(DiasQueAplica diasQueAplica) {
		this.diasQueAplica = diasQueAplica;
	}
	
}
