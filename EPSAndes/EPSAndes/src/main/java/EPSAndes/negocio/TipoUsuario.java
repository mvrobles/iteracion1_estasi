package EPSAndes.negocio;

public class TipoUsuario {

	//----------------------------------------------
	//               ATRIBUTOS
	//----------------------------------------------
	
	private long idTipoUsuario;
	private String tipoUsuario;
	
	public TipoUsuario(long idTipoUsuario, String tipoUsuario)
	{
		setIdTipoUsuario(idTipoUsuario);
		setTipoUsuario(tipoUsuario);
	}
	
	public long getIdTipoUsuario() {
		return idTipoUsuario;
	}
	public void setIdTipoUsuario(long idTipoUsuario) {
		this.idTipoUsuario = idTipoUsuario;
	}
	public String getTipoUsuario() {
		return tipoUsuario;
	}
	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	
	
}
