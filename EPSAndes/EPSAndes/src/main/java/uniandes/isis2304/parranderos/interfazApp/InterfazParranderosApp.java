/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Universidad	de	los	Andes	(Bogotá	- Colombia)
 * Departamento	de	Ingeniería	de	Sistemas	y	Computación
 * Licenciado	bajo	el	esquema	Academic Free License versión 2.1
 * 		
 * Curso: isis2304 - Sistemas Transaccionales
 * Proyecto: Parranderos Uniandes
 * @version 1.0
 * @author Germán Bravo
 * Julio de 2018
 * 
 * Revisado por: Claudia Jiménez, Christian Ariza
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

package uniandes.isis2304.parranderos.interfazApp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

import javax.jdo.JDODataStoreException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

import EPSAndes.negocio.EPSAndes;
import uniandes.isis2304.parranderos.negocio.Parranderos;
import uniandes.isis2304.parranderos.negocio.VOTipoBebida;

/**
 * Clase principal de la interfaz
 * @author Germán Bravo
 */
@SuppressWarnings("serial")

public class InterfazParranderosApp extends JFrame implements ActionListener
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Logger para escribir la traza de la ejecución
	 */
	private static Logger log = Logger.getLogger(InterfazParranderosApp.class.getName());

	/**
	 * Ruta al archivo de configuración de la interfaz
	 */
	private static final String CONFIG_INTERFAZ = "./src/main/resources/config/interfaceConfigApp.json"; 

	/**
	 * Ruta al archivo de configuración de los nombres de tablas de la base de datos
	 */
	private static final String CONFIG_TABLAS = "./src/main/resources/config/TablasBD_A.json"; 

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * Objeto JSON con los nombres de las tablas de la base de datos que se quieren utilizar
	 */
	private JsonObject tableConfig;

	/**
	 * Asociación a la clase principal del negocio.
	 */
	private EPSAndes parranderos;

	/* ****************************************************************
	 * 			Atributos de interfaz
	 *****************************************************************/
	/**
	 * Objeto JSON con la configuración de interfaz de la app.
	 */
	private JsonObject guiConfig;

	/**
	 * Panel de despliegue de interacción para los requerimientos
	 */
	private PanelDatos panelDatos;

	/**
	 * Menú de la aplicación
	 */
	private JMenuBar menuBar;

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Construye la ventana principal de la aplicación. <br>
	 * <b>post:</b> Todos los componentes de la interfaz fueron inicializados.
	 */
	public InterfazParranderosApp( )
	{
		// Carga la configuración de la interfaz desde un archivo JSON
		guiConfig = openConfig ("Interfaz", CONFIG_INTERFAZ);

		// Configura la apariencia del frame que contiene la interfaz gráfica
		configurarFrame ( );
		if (guiConfig != null) 	   
		{
			crearMenu( guiConfig.getAsJsonArray("menuBar") );
		}

		tableConfig = openConfig ("Tablas BD", CONFIG_TABLAS);
		parranderos = new EPSAndes (tableConfig);

		String path = guiConfig.get("bannerPath").getAsString();
		panelDatos = new PanelDatos ( );

		setLayout (new BorderLayout());
		add (new JLabel (new ImageIcon (path)), BorderLayout.NORTH );          
		add( panelDatos, BorderLayout.CENTER );        
	}

	/* ****************************************************************
	 * 			Métodos de configuración de la interfaz
	 *****************************************************************/
	/**
	 * Lee datos de configuración para la aplicació, a partir de un archivo JSON o con valores por defecto si hay errores.
	 * @param tipo - El tipo de configuración deseada
	 * @param archConfig - Archivo Json que contiene la configuración
	 * @return Un objeto JSON con la configuración del tipo especificado
	 * 			NULL si hay un error en el archivo.
	 */
	private JsonObject openConfig (String tipo, String archConfig)
	{
		JsonObject config = null;
		try 
		{
			Gson gson = new Gson( );
			FileReader file = new FileReader (archConfig);
			JsonReader reader = new JsonReader ( file );
			config = gson.fromJson(reader, JsonObject.class);
			log.info ("Se encontró un archivo de configuración válido: " + tipo);
		} 
		catch (Exception e)
		{
			//			e.printStackTrace ();
			log.info ("NO se encontró un archivo de configuración válido");			
			JOptionPane.showMessageDialog(null, "No se encontró un archivo de configuración de interfaz válido: " + tipo, "Parranderos App", JOptionPane.ERROR_MESSAGE);
		}	
		return config;
	}

	/**
	 * Método para configurar el frame principal de la aplicación
	 */
	private void configurarFrame(  )
	{
		int alto = 0;
		int ancho = 0;
		String titulo = "";	

		if ( guiConfig == null )
		{
			log.info ( "Se aplica configuración por defecto" );			
			titulo = "Parranderos APP Default";
			alto = 300;
			ancho = 500;
		}
		else
		{
			log.info ( "Se aplica configuración indicada en el archivo de configuración" );
			titulo = guiConfig.get("title").getAsString();
			alto= guiConfig.get("frameH").getAsInt();
			ancho = guiConfig.get("frameW").getAsInt();
		}

		setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		setLocation (50,50);
		setResizable( true );
		setBackground( Color.WHITE );

		setTitle( titulo );
		setSize ( ancho, alto);        
	}

	/**
	 * Método para crear el menú de la aplicación con base em el objeto JSON leído
	 * Genera una barra de menú y los menús con sus respectivas opciones
	 * @param jsonMenu - Arreglo Json con los menùs deseados
	 */
	private void crearMenu(  JsonArray jsonMenu )
	{    	
		// Creación de la barra de menús
		menuBar = new JMenuBar();       
		for (JsonElement men : jsonMenu)
		{
			// Creación de cada uno de los menús
			JsonObject jom = men.getAsJsonObject(); 

			String menuTitle = jom.get("menuTitle").getAsString();        	
			JsonArray opciones = jom.getAsJsonArray("options");

			JMenu menu = new JMenu( menuTitle);

			for (JsonElement op : opciones)
			{       	
				// Creación de cada una de las opciones del menú
				JsonObject jo = op.getAsJsonObject(); 
				String lb =   jo.get("label").getAsString();
				String event = jo.get("event").getAsString();

				JMenuItem mItem = new JMenuItem( lb );
				mItem.addActionListener( this );
				mItem.setActionCommand(event);

				menu.add(mItem);
			}       
			menuBar.add( menu );
		}        
		setJMenuBar ( menuBar );	
	}

	/* ****************************************************************
	 * 			CRUD de TipoBebida
	 *****************************************************************/
	//    /**
	//     * Adiciona un tipo de bebida con la información dada por el usuario
	//     * Se crea una nueva tupla de tipoBebida en la base de datos, si un tipo de bebida con ese nombre no existía
	//     */
	//    public void adicionarTipoBebida( )
	//    {
	//    	try 
	//    	{
	//    		String nombreTipo = JOptionPane.showInputDialog (this, "Nombre del tipo de bedida?", "Adicionar tipo de bebida", JOptionPane.QUESTION_MESSAGE);
	//    		if (nombreTipo != null)
	//    		{
	//        		VOTipoBebida tb = parranderos.adicionarTipoBebida (nombreTipo);
	//        		if (tb == null)
	//        		{
	//        			throw new Exception ("No se pudo crear un tipo de bebida con nombre: " + nombreTipo);
	//        		}
	//        		String resultado = "En adicionarTipoBebida\n\n";
	//        		resultado += "Tipo de bebida adicionado exitosamente: " + tb;
	//    			resultado += "\n Operación terminada";
	//    			panelDatos.actualizarInterfaz(resultado);
	//    		}
	//    		else
	//    		{
	//    			panelDatos.actualizarInterfaz("Operación cancelada por el usuario");
	//    		}
	//		} 
	//    	catch (Exception e) 
	//    	{
	////			e.printStackTrace();
	//			String resultado = generarMensajeError(e);
	//			panelDatos.actualizarInterfaz(resultado);
	//		}
	//    }
	//
	//    /**
	//     * Consulta en la base de datos los tipos de bebida existentes y los muestra en el panel de datos de la aplicación
	//     */
	//    public void listarTipoBebida( )
	//    {
	//    	try 
	//    	{
	//			List <VOTipoBebida> lista = parranderos.darVOTiposBebida();
	//
	//			String resultado = "En listarTipoBebida";
	//			resultado +=  "\n" + listarTiposBebida (lista);
	//			panelDatos.actualizarInterfaz(resultado);
	//			resultado += "\n Operación terminada";
	//		} 
	//    	catch (Exception e) 
	//    	{
	////			e.printStackTrace();
	//			String resultado = generarMensajeError(e);
	//			panelDatos.actualizarInterfaz(resultado);
	//		}
	//    }
	//
	//    /**
	//     * Borra de la base de datos el tipo de bebida con el identificador dado po el usuario
	//     * Cuando dicho tipo de bebida no existe, se indica que se borraron 0 registros de la base de datos
	//     */
	//    public void eliminarTipoBebidaPorId( )
	//    {
	//    	try 
	//    	{
	//    		String idTipoStr = JOptionPane.showInputDialog (this, "Id del tipo de bedida?", "Borrar tipo de bebida por Id", JOptionPane.QUESTION_MESSAGE);
	//    		if (idTipoStr != null)
	//    		{
	//    			long idTipo = Long.valueOf (idTipoStr);
	//    			long tbEliminados = parranderos.eliminarTipoBebidaPorId (idTipo);
	//
	//    			String resultado = "En eliminar TipoBebida\n\n";
	//    			resultado += tbEliminados + " Tipos de bebida eliminados\n";
	//    			resultado += "\n Operación terminada";
	//    			panelDatos.actualizarInterfaz(resultado);
	//    		}
	//    		else
	//    		{
	//    			panelDatos.actualizarInterfaz("Operación cancelada por el usuario");
	//    		}
	//		} 
	//    	catch (Exception e) 
	//    	{
	////			e.printStackTrace();
	//			String resultado = generarMensajeError(e);
	//			panelDatos.actualizarInterfaz(resultado);
	//		}
	//    }
	//
	//    /**
	//     * Busca el tipo de bebida con el nombre indicado por el usuario y lo muestra en el panel de datos
	//     */
	//    public void buscarTipoBebidaPorNombre( )
	//    {
	//    	try 
	//    	{
	//    		String nombreTb = JOptionPane.showInputDialog (this, "Nombre del tipo de bedida?", "Buscar tipo de bebida por nombre", JOptionPane.QUESTION_MESSAGE);
	//    		if (nombreTb != null)
	//    		{
	//    			VOTipoBebida tipoBebida = parranderos.darTipoBebidaPorNombre (nombreTb);
	//    			String resultado = "En buscar Tipo Bebida por nombre\n\n";
	//    			if (tipoBebida != null)
	//    			{
	//        			resultado += "El tipo de bebida es: " + tipoBebida;
	//    			}
	//    			else
	//    			{
	//        			resultado += "Un tipo de bebida con nombre: " + nombreTb + " NO EXISTE\n";    				
	//    			}
	//    			resultado += "\n Operación terminada";
	//    			panelDatos.actualizarInterfaz(resultado);
	//    		}
	//    		else
	//    		{
	//    			panelDatos.actualizarInterfaz("Operación cancelada por el usuario");
	//    		}
	//		} 
	//    	catch (Exception e) 
	//    	{
	////			e.printStackTrace();
	//			String resultado = generarMensajeError(e);
	//			panelDatos.actualizarInterfaz(resultado);
	//		}
	//    }
	
	/* ****************************************************************
	 * 			Métodos de la Interacción
	 *****************************************************************/
	/**
	 * Método para la ejecución de los eventos que enlazan el menú con los métodos de negocio
	 * Invoca al método correspondiente según el evento recibido
	 * @param pEvento - El evento del usuario
	 */
	@Override
	public void actionPerformed(ActionEvent pEvento)
	{
		String evento = pEvento.getActionCommand( );		
		try 
		{
			Method req = InterfazParranderosApp.class.getMethod ( evento );			
			req.invoke ( this );
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}

	/* ****************************************************************
	 * 			Programa principal
	 *****************************************************************/
	/**
	 * Este método ejecuta la aplicación, creando una nueva interfaz
	 * @param args Arreglo de argumentos que se recibe por línea de comandos
	 */
	public static void main( String[] args )
	{
		try
		{

			// Unifica la interfaz para Mac y para Windows.
			UIManager.setLookAndFeel( UIManager.getCrossPlatformLookAndFeelClassName( ) );
			InterfazParranderosApp interfaz = new InterfazParranderosApp( );
			interfaz.setVisible( true );
		}
		catch( Exception e )
		{
			e.printStackTrace( );
		}
	}
}
