package uniandes.isis2304.parranderos.persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

class SQLUsuario {

	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra ac� para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaEPSAndes.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicaci�n
	 */
	private PersistenciaEPSAndes pp;

	/* ****************************************************************
	 * 			M�todos
	 *****************************************************************/

	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicaci�n
	 */
	public SQLUsuario (PersistenciaEPSAndes pp)
	{
		this.pp = pp;
	}

	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un USUARIO a la base de datos de EPSAndes
	 * @param pm - El manejador de persistencia
	 * @param  numeroDocumento- El identificador del usuario
	 * @param tipoDocumento - El tipo de documento del usuario que puede ser TARJETA_IDENTIDAD
	 * 																		 CEDULA_CIUDADANIA
	 * 																		 CEDULA_EXTRANJERIA						
	 * @param nombre - EL nombre del usuario a agregar
	 * @param contrasenia - La constraseña del usuario
	 * @param rol - El rol del usuario, puede ser 1 para AFILIADO
	 * 											  2 para GERENTE
	 * 											  3 para ADMINISTRADOR_DE_DATOS
	 * 											  4 para MEDICO
	 * 										      5 para RECEPCIONISTA
	 * @return El n�mero de tuplas insertadas
	 */
	public long adicionarUsuario (PersistenceManager pm, String tipoDocumento, long numeroDocumento, String nombre, String correoElectronico, String contrasenia, long idRol) 
	{
		Query q = pm.newQuery(SQL, "INSERT INTO " + pp.darTablaUsuario () + "(numeroDocumento, tipoDocumento, nombre, correoElectronico, contrasenia, idRol) values (?, ?, ?, ?, ?, ?)");
		q.setParameters(numeroDocumento, tipoDocumento, nombre, correoElectronico, contrasenia, idRol);
		return (long) q.executeUnique();
	}
	

}
