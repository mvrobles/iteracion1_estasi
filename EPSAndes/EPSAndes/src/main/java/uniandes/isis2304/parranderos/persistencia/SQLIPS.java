package uniandes.isis2304.parranderos.persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

class SQLIPS {

	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra ac� para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaEPSAndes.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicaci�n
	 */
	private PersistenciaEPSAndes pp;

	/* ****************************************************************
	 * 			M�todos
	 *****************************************************************/

	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicaci�n
	 */
	public SQLIPS (PersistenciaEPSAndes pp)
	{
		this.pp = pp;
	}

	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un USUARIO a la base de datos de EPSAndes
	 * @param pm - El manejador de persistencia
	 * @param idIPS- El identificador de IPS
	 * @param nombre - El nombre de la IPS				
	 * @param localizacion - La localizacion de la IPS
	 * @return El n�mero de tuplas insertadas
	 */
	public long registrarIPS (PersistenceManager pm, Integer idIPS, String nombre, String localizacion) 
	{
		Query q = pm.newQuery(SQL, "INSERT INTO " + pp.darTablaIPS () + "(idIPS, nombre, localizacion) values (?, ?, ?, ?, ?)");
		q.setParameters(idIPS, nombre, localizacion);
		return (long) q.executeUnique();
	}
}
